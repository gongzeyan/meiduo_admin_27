from rest_framework.viewsets import ModelViewSet

from apps.goods.models import SKU
from apps.meiduo_admin.serializers.sku import SKUSerializer, GoodsCategorySerializer, SPUSerializer
from apps.meiduo_admin.utils import PageNum


class SKUModelViewSet(ModelViewSet):

    # queryset = SKU.objects.all()
    def get_queryset(self):

        keyword=self.request.query_params.get('keyword')

        if keyword:
            return SKU.objects.filter(name__contains=keyword)

        return SKU.objects.all()

    serializer_class = SKUSerializer

    pagination_class = PageNum

"""
获取三级分类数据
GoodsCategory
GoodsCategory.objects.filter(parent_id__gt=37)
GoodsCategory.objects.filter(subs=None)
"""
from apps.goods.models import GoodsCategory
from rest_framework.generics import ListAPIView
class ThreeCategoryListAPIView(ListAPIView):

    queryset = GoodsCategory.objects.filter(parent_id__gt=37)

    serializer_class = GoodsCategorySerializer

"""
获取所有的SPU的数据
"""
from apps.goods.models import SPU
class SPUListAPIView(ListAPIView):

    queryset = SPU.objects.all()

    serializer_class = SPUSerializer