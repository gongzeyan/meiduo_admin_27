from django.conf.urls import url

#导入 jwt的视图
from rest_framework_jwt.views import obtain_jwt_token

from .views import statistical
from .views import users
from .views import image
from .views import sku

#########################图片路由#########################################
urlpatterns = [

    url(r'^authorizations/$', obtain_jwt_token),

    ######################统计###############################
    url(r'^statistical/total_count/$', statistical.UserAllCountAPIView.as_view()),

    url(r'^statistical/day_increment/$', statistical.UserDayAddCountAPIView.as_view()),


    url(r'^statistical/day_active/$', statistical.UserDayActiveCountAPIView.as_view()),


    url(r'^statistical/day_orders/$', statistical.UserDayOrdersCountAPIView.as_view()),

    url(r'^statistical/month_increment/$', statistical.UserMonthCountAPIView.as_view()),

    url(r'^statistical/goods_day_views/$', statistical.GoodsDayViewCountAPIView.as_view()),


    ########################User相关###################################
    url(r'^users/$', users.UsersListAPIView.as_view()),

    ####################图片相关的###########################

    url(r'^skus/simple/$', image.SimpleSKUListAPIView.as_view()),


    ##################sku相关##########################################
    url(r'^skus/categories/$', sku.ThreeCategoryListAPIView.as_view()),
    url(r'^goods/simple/$', sku.SPUListAPIView.as_view()),



    #url(r'^authorizations/$',views.AdminLoginAPIView.as_view()),
]
#图片的管理视图集的url
from rest_framework.routers import DefaultRouter

#创建router实例对象
router = DefaultRouter()
#注册路由
router.register(r'skus/images',image.ImageModelViewSet,basename='images')
#将路由追加到urlpatterns
urlpatterns += router.urls


#############################SKU路由#######################################
router = DefaultRouter()

router.register(r'skus',sku.SKUModelViewSet,basename='skus')

urlpatterns += router.urls

